The first prototype for Steve. A hard-coded HTML mockup of the proposed web application.

WALKTHROUGH
	Start by opening home.html.

	HOMEPAGE (home.html):
		A very incomplete mockup of the homepage which does contain a complete navigation bar.
		Clicking the 'Project Title' button will redirect you to the homepage.
		Clicking the 'Login' button will redirect you to the login page.
		Clicking the 'Signup' button will redirect you to the signup page.

	SIGNUP (signup.html):
		A simple mockup of the registration form which harbours dynamic text fields which change upon selecting
		a user type.
		The 'Signup' button at the bottom of the registration form currently has no effect.

	LOGIN (login.html):
		A tab-based login screen which allows you to select a login method by clicking one of the tabs.
		Clicking the 'Login' button regardless of selected login method and entered fields will redirect
		you to the user dashboard.

	DASHBOARD (dashboard.html):
		An interactive mockup of the proposed dashboard. Favourited users are categorised based off user type.
		Clicking the various displayed circles will display a list of users from that category. Clicking these
		names will show additional info about the user.
		Clicking 'Search' will redirect you to the placeholder search results regardless of what's entered in
		the search bar.
		Clicking 'Dashboard' will redirect you to the dashboard.
		Clicking 'Settings' will redirect you the settings page.

	SEARCH RESULTS (results.html):
		A non-interactive mockup of the search results page.
		The 'Add' button currently has no functionality but	will be used to add a user to the favourites list.

	SETTINGS (settings.html):
		An interactive vertical tab-based menu which displays submenus based off the user's selection.
		Clicking 'Account' will display email and password fields akin to the registration form used for email
		and password changing.
		Clicking 'Profile' will display a placeholder submenu but is planned to contain user type selection and
		appropriate user information fields akin to the registration form.
		Clicking 'Dashboard' will display a placeholder submenu that is planned to contain settings regarding
		the favourites list and news feed.
		Clicking 'Search' will display a placeholder submenu that is planned to contain options regarding the
		search functionality.
