GENERAL
	- Separate CSS and JS into separate files (or save this for the next prototype)
	- Add comments to all files

HOMEPAGE
	- Stylise about sections
	- Add fancy scroll thing

SIGNUP
	- Stylise form
	- Make form functional and optimised
	- Make appropriate fields (e.g. larger text field for additional info etc.)

DASHBOARD
	- Stylise actual dashboard

SETTINGS
	- Add individual settings

ADVANCED SEARCH
	- Add search form
